import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        ThreadSimulator t1 = new ThreadSimulator("t1", 100, 10);
        ThreadSimulator t2 = new ThreadSimulator("t2", 300, 0);
        ThreadSimulator t3 = new ThreadSimulator("t3", 400, 25);
        ThreadSimulator t4 = new ThreadSimulator("t4", 200, 17);
        ThreadSimulator t5 = new ThreadSimulator("t5", 200, 19);
        List<ThreadSimulator> list = new ArrayList<>();
        list.add(t1);
        list.add(t2);
        list.add(t3);
        list.add(t4);
        list.add(t5);
        list.sort(Comparator.comparingInt(ThreadSimulator::getArrivalTime));
        int count = 1;
        int executionEndCounter = 0;
        boolean  continueLoop = true;
        int CPUTime = 100;
        while (continueLoop) {
            System.out.println("Step " + count);
            if(executionEndCounter == list.size()) {
                continueLoop = false;
            }
            for (ThreadSimulator thread:list) {
                if(CPUTime >= thread.totalTimeToExecute) {
                    thread.executedTime += CPUTime - thread.totalTimeToExecute;
                } else {
                    if(thread.executedTime < thread.totalTimeToExecute) {
                        thread.executedTime += CPUTime;
                    }
                }

                if(thread.totalTimeToExecute == thread.executedTime) {
                    ++executionEndCounter;
                }
                System.out.println("NAME: " + thread.name + "  EXECUTED TIME: " + thread.executedTime +
                        "  TOTAL TIME TO EXECUTE: " + thread.totalTimeToExecute);
            }
            ++count;
        }

    }
}

