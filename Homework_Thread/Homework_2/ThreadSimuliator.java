public class ThreadSimulator  {
    public String name;
    public int executedTime;
    public int totalTimeToExecute;
    public int arrivalTime;

    ThreadSimulator(String name, int timeToExecute, int arrivalTime) {
        this.name = name;
        this.totalTimeToExecute = timeToExecute;
        executedTime = 0;
        this.arrivalTime = arrivalTime;
    }
    public int getArrivalTime(){
        return this.arrivalTime;
    }
}

