public class ThreadsWith2Flags {
    public static int balance = 10;
    public static boolean actionFlag;
    public static boolean printFlag;
    public static void main(String[] args) throws InterruptedException {

        Thread producer = new Thread(new Runnable() {
            @Override
            public void run()
            {
                while(true) {
                    if(!actionFlag) {
                        if (balance != 10) {
                            balance += 10;
                            if(!printFlag) {
                                System.out.println("Increased" + balance);
                            }
                            printFlag = true;
                        }
                    }
                    actionFlag = true;
                }
            }
        });

        Thread consumer = new Thread(new Runnable() {
            @Override
            public void run()
            {
                while(true) {
                    if(actionFlag) {
                        if (balance != 0) {
                            balance -= 10;
                            if(printFlag) {
                                System.out.println("Decreased" + balance);
                            }
                            printFlag = false;
                        }
                    }
                    actionFlag = false;
                }
            }
        });
        producer.start();
        consumer.start();
    }
}
