import java.util.ArrayList;
import java.util.List;

class PC {
private List<Integer> list = new ArrayList<>(10);
    private int capacity = 0;
    int value = 0;

    void producer() throws InterruptedException {
        while (true) synchronized (this) {
            while (list.size() == 10) {
                wait();
            }

            list.add(value);
            System.out.println("producer produced: " + list.get(value));
            value++;

            notify();
            Thread.sleep(500);
        }
    }

    void consumer() throws InterruptedException {
        while (true) { synchronized (this) {
            if (list.size() == 0) {
                wait();
            }
            value--;
            System.out.println("consumer consumed: " + list.get(value));
            list.remove(value);

            notify();
            Thread.sleep(500);
        }

        }
    }
}
