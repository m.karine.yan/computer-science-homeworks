import java.util.LinkedList;

public class Threads {
    public static void main(String[] args) throws InterruptedException {
        final PC pc = new PC();
        Thread producer = new Thread(new Runnable() {
            @Override
            public void run()
            {
                try {
                    pc.producer();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread consumer = new Thread(new Runnable() {
            @Override
            public void run()
            {
                try {
                    pc.consumer();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        producer.start();
        consumer.start();

        producer.join();
        consumer.join();
    }
}

